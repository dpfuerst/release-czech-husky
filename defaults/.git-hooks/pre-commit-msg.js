const { exec: executeCommand } = require("child_process");

const isLinux =
  process.platform === "cygwin" ||
  process.platform === "freebsd" ||
  process.platform === "linux" ||
  process.platform === "netbsd" ||
  process.platform === "openbsd";
const isMacOS = process.platform === "darwin";
const isWindows = process.platform === "win32";

const gitHookOnLinux =
  "exec < /dev/tty && node_modules/.bin/git-cz --hook || true";
const gitHookOnMacOS = 'osascript .git-hooks/pre-commit-msg.scpt "$PWD"';
const gitHookOnWindows =
  'start "Git Commit" "%ProgramFiles%\\Git\\git-bash.exe" node_modules/.bin/git-cz --hook || true';

let gitHook;

if (isLinux) {
  gitHook = gitHookOnLinux;
} else if (isMacOS) {
  gitHook = gitHookOnMacOS;
} else if (isWindows) {
  gitHook = gitHookOnWindows;
} else {
  process.exit(1);
}

executeCommand(gitHook, error => {
  if (error) console.error(`\x1b[31m${error}\x1b[0m`);
});
