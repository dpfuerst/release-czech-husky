on run argv
	set repositoryPath to quoted form of (item 1 of argv)
	
	tell application "iTerm"
		set terminalWindow to (create window with default profile)
		set terminalSession to (get current session of terminalWindow)
		
		delay 0.75 # Wait for the application to open.
		
		tell application "System Events" to tell process "iTerm2"
			set value of attribute "AXFullScreen" of window (get index of terminalWindow) to true
		end tell
		
		delay 0.3 # Wait for the application to transition to presentation mode (i.e. fullscreen).
		
		# `... && exit 0` fails if `git commit --no-edit ...` fails. Hence, the whole left-outermost expression (i.e. to the left of `||`) fails. Consequently, `exit 1` is executed and the commit is aborted.
		write terminalSession text "cd " & repositoryPath & " && (node_modules/.bin/git-cz --hook || true) && exit 0"
	end tell
	
	tell application "System Events"
		repeat while terminalWindow exists
			delay 1
		end repeat
		
		tell process "Fork"
			set frontmost to true
			perform action "AXRaise" of (windows whose title starts with "Fork")
		end tell
	end tell
end run


