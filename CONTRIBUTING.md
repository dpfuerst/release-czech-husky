# Contributing to `release-czech-husky`

You can feel free to suggest and implement a new feature or improve an existing feature by opening an [issue](https://gitlab.com/dpfuerst/release-czech-husky/issues/new?issue) or a [merge request](https://gitlab.com/dpfuerst/release-czech-husky/merge_requests/new).
