# `release-czech-husky`

A wrapper for [`husky`](https://github.com/typicode/husky), [`git-cz`](https://github.com/streamich/git-cz), and [`semantic-release`](https://github.com/semantic-release/semantic-release#default-commit-message-format) to unleash the united thrust these gems provide.

## Installation

Install this package with:

```bash
npm i -D @limanio/release-czech-husky
```

and satisfy its peer dependencies (_required!_) with:

```bash
npm i -D @semantic-release/changelog@3 @semantic-release/git@8 @semantic-release/gitlab@4 conventional-changelog-conventionalcommits@4 git-cz@4 husky@4 semantic-release@16
```

Run the setup of this package with:

```bash
./node_modules/.bin/release-czech-husky install
```

Lastly, configure the `git commit` hook with:

```json
"husky": {
  "hooks": {
    "prepare-commit-msg": "node .git-hooks/pre-commit-msg.js"
  }
}
```

in `package.json`.

## Update

Update this package with:

```bash
npm i -D @limanio/release-czech-husky@latest
```

### <a name="update-configuration"></a> Configuration

Update the configuration of this package with:

```bash
./node_modules/.bin/release-czech-husky update
```

This will setup new configuration files and prompt you whether to reset already existing configuration files. To forcefully overwrite those, instead, run:

```bash
./node_modules/.bin/release-czech-husky update --overwrite
```

### Git Hook for macOS

The Git hook for macOS is automatically recompiled during the [update of the configuration](#update-configuration). In case you want to update the [default Git hook for macOS](./defaults/.git-hooks/pre-commit-msg.applescript) yourself, you can run

```bash
./node_modules/.bin/release-czech-husky update --mac-os
```

to recompile it.

## Configuration

### `husky`

You can configure `husky` as [usually](https://github.com/typicode/husky#guides) and fine-tune the predefined `git commit` hook by editing `.git-hooks/pre-commit-msg.js` and `.git-hooks/pre-commit-msg.applescript` on macOS.

### `git-cz`

You can configure `git-cz` as [usually](https://github.com/streamich/git-cz#custom-config) in `changelog.config.js`.

### `semantic-release`

You can configure `semantic-release` as [usually](https://github.com/semantic-release/semantic-release/blob/master/docs/usage/configuration.md#configuration) in `release.config.js`.

### GitLab CI/CD

You can configure the _GitLab CI/CD_ as [usually](https://docs.gitlab.com/ee/ci/#configuration) in `.gitlab-ci.yml`.

### GitLab CI/CD Token

`@semantic-release/gitlab` used during deployment [requires](https://github.com/semantic-release/gitlab#environment-variables) the environment variable `GITLAB_TOKEN` to be present during execution. Most easily, you can configure [the variables of GitLab CI/CD](https://docs.gitlab.com/ee/ci/variables/README.html) for this purpose. It is recommended to set the variable as _protected_ and _masked_ due to its confidential nature.

## Opinionated Defaults

### `git commit` hook on Windows

Under Windows, the `git commit` hook assumes that Git is installed at `%ProgramFiles%\Git`.

### `git commit` hook on macOS

Under macOS, the `git commit` hook assumes the use of [Fork](https://fork.dev/) and [iTerm 2](https://iterm2.com/).

### Deployment with GitLab

The deployment with [GitLab CI/CD](https://docs.gitlab.com/ee/ci/) is assumed.

## FAQ

### Why does the version in `package.json` and `package-lock.json` not get updated when running the GitLab CI/CD pipeline?

The package `@semantic-release/npm` is responsible for updating the version in `package.json` and `package-lock.json`. That package is not included as step in the default configuration of `semantic-release`.

#### Why is `@semantic-release/npm` not included in the default configuration of `semantic-release`?

By default, `@semantic-release/npm` attempts to publish your package to the npm registry. As the GitLab CI/CD pipeline lacks the environment variable necessary to execute this step during deployment, the pipeline would fail.  
If you still desire to update the version in `package.json` and `package-lock.json`, you can include `@semantic-release/npm` before `@semantic-release/git` and add `"private": true` to your `package.json`. This way, your package does not get accidentally published to the npm registry and your GitLab CI/CD pipeline does not require `NPM_TOKEN` to be set.

### With `@semantic-release/npm` included in my configuration, why does the version in `package.json` and `package-lock.json` within a nested npm package not get updated when running the GitLab CI/CD pipeline?

By default, `@semantic-release/npm` updates the version of `package.json` and `package-lock.json` found in `.`, i.e. the root of your project. On top of that, `@semantic-release/npm` can only handle one pair of `package.json` and `package-lock.json`. You can still update any number of nested npm packages, by using the [`postpublish` `npm-script`](https://docs.npmjs.com/misc/scripts):

```json
"scripts": {
  "postpublish": "cd nested/package && npm version $(node -pe 'require('../../package.json').version') && cd ../.."
}
```
