export const compiledGitHookForMacOS: string = 'pre-commit-msg.scpt';
export const defaults: string =
  'node_modules/@limanio/release-czech-husky/defaults';
export const gitCZConfiguration: string = 'changelog.config.js';
export const gitHookBootstrap: string = 'pre-commit-msg.js';
export const gitHookDirectory: string = '.git-hooks';
export const gitHookForMacOS: string = 'pre-commit-msg.applescript';
export const gitLabConfiguration: string = '.gitlab-ci.yml';
export const semanticReleaseConfiguration: string = 'release.config.js';
