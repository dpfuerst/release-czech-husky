import chalk from 'chalk';
import { execSync } from 'child_process';
import fileSystem from 'fs';
import path from 'path';
import prompts, { Answers } from 'prompts';
import walk, { Walker, WalkNext, WalkStats } from 'walk';
import {
  compiledGitHookForMacOS,
  defaults,
  gitCZConfiguration,
  gitHookBootstrap,
  gitHookDirectory,
  gitHookForMacOS,
  gitLabConfiguration,
  semanticReleaseConfiguration
} from './filesAndFolders';
import { releaseCzechHusky } from './formattedStrings';

interface Tool {
  name: string;
  emoji: string;
  path: string[];
  afterCopy?: () => void;
}

const gitHookCompilationCommand: string = `osacompile -o ${path.join(
  gitHookDirectory,
  compiledGitHookForMacOS
)} ${path.join(gitHookDirectory, gitHookForMacOS)}`;

const conflictingFiles: string[] = [];
const tools: Tool[] = [
  {
    name: 'husky',
    emoji: '🐶',
    path: [defaults, gitHookDirectory, gitHookBootstrap]
  },
  {
    name: 'husky for macOS',
    emoji: '🐶',
    path: [defaults, gitHookDirectory, gitHookForMacOS],
    afterCopy: () => execSync(gitHookCompilationCommand)
  },
  {
    name: 'git-cz',
    emoji: '🇨🇿',
    path: [defaults, gitCZConfiguration]
  },
  {
    name: 'semantic-release',
    emoji: '🚀',
    path: [defaults, semanticReleaseConfiguration]
  },
  {
    name: 'GitLab CI/CD',
    emoji: '🦊',
    path: [defaults, gitLabConfiguration]
  }
];
export default async (options: string[]): Promise<void> => {
  // tslint:disable-next-line: no-console
  console.log(`\nUpdating ${releaseCzechHusky}...`);

  try {
    if (options.length === 1 && options[0] === '--mac-os') {
      execSync(gitHookCompilationCommand);
    } else {
      if (options.length !== 1 || options[0] !== '--overwrite')
        await detectConflictingFiles();

      for (const tool of tools) {
        await update(tool);
      }
    }
  } catch (error) {
    throw error;
  }

  // tslint:disable-next-line: no-console
  console.log(`Successfully updated ${releaseCzechHusky}.`);
};

const detectConflictingFiles = (): Promise<void> =>
  new Promise((resolve, reject) => {
    const walker: Walker = walk.walk(defaults);

    walker.on(
      'file',
      (root: string, fileStatistics: WalkStats, next: WalkNext) => {
        try {
          if (fileSystem.existsSync(path.join(root, fileStatistics.name)))
            conflictingFiles.push(fileStatistics.name);
        } catch (error) {
          reject(error);
        }

        next();
      }
    );

    walker.on('errors', (root: string, errorStatistics: WalkStats[]) => {
      reject(
        new Error(
          chalk`{red Failed to prepare files for update:} ${errorStatistics} (at ${root})`
        )
      );
    });

    walker.on('end', resolve);
  });

const update = async (tool: Tool) => {
  const configurationConflicts: boolean =
    conflictingFiles.indexOf(gitCZConfiguration) !== -1;
  const configurationFile = tool.path[tool.path.length - 1];
  let response: Answers<'overwrite'> | undefined;

  // tslint:disable-next-line: no-console
  console.log(`Updating ${tool.name}...${configurationConflicts ? '\n' : ''}`);

  if (configurationConflicts) {
    response = await prompts({
      type: 'confirm',
      name: 'overwrite',
      message: chalk`Do you want to set the {italic ${tool.name}} configuration ({bold.red ${configurationFile}}) to the default?`
    });
  }

  if (response?.overwrite || !configurationConflicts) {
    try {
      fileSystem.copyFileSync(
        path.join(...tool.path),
        path.join(...tool.path.slice(1))
      );

      if (tool.afterCopy && typeof tool.afterCopy === 'function')
        tool.afterCopy();
    } catch (error) {
      throw error;
    }
  }

  // tslint:disable-next-line: no-console
  console.log(
    `${configurationConflicts ? '\n' : ''}Successfully set up ${tool.name}. ${
      tool.emoji
    }`
  );
};
