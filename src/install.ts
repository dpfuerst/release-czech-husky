import { execSync } from 'child_process';
import fileSystem from 'fs';
import ncp from 'ncp';
import {
  defaults,
  gitCZConfiguration,
  gitHookDirectory,
  gitLabConfiguration,
  semanticReleaseConfiguration
} from './filesAndFolders';
import {
  gitCZ,
  husky,
  releaseCzechHusky,
  semanticRelease
} from './formattedStrings';

const gitHookCompilationCommand: string =
  'osacompile -o .git-hooks/pre-commit-msg.scpt .git-hooks/pre-commit-msg.applescript';
const isMacOS: boolean = process.platform === 'darwin';

export default async (): Promise<void> => {
  // tslint:disable-next-line: no-console
  console.log(`\nInstalling ${releaseCzechHusky}...`);

  try {
    await setupHusky();
    setupHuskyForMacOS();
    setupGitCZ();
    setupSemanticRelease();
  } catch (error) {
    throw error;
  }

  // tslint:disable-next-line: no-console
  console.log(`Successfully installed ${releaseCzechHusky}.`);
};

const setupHusky = (): Promise<void> =>
  new Promise((resolve, reject) => {
    // tslint:disable-next-line: no-console
    console.log(`\nSetting up ${husky}...`);

    ncp(`${defaults}/${gitHookDirectory}`, gitHookDirectory, error => {
      if (error) {
        reject(error);
      } else {
        resolve();
      }
    });
  });

const setupHuskyForMacOS = (): void => {
  if (isMacOS) {
    try {
      execSync(gitHookCompilationCommand);
    } catch (error) {
      throw error;
    }
  }

  // tslint:disable-next-line: no-console
  console.log(`Successfully set up ${husky}. 🐶\n`);
};

const setupGitCZ = (): void => {
  // tslint:disable-next-line: no-console
  console.log(`Setting up ${gitCZ}...`);

  try {
    fileSystem.copyFileSync(
      `${defaults}/${gitCZConfiguration}`,
      gitCZConfiguration
    );
  } catch (error) {
    throw error;
  }

  // tslint:disable-next-line: no-console
  console.log(`Successfully set up ${gitCZ}. 🇨🇿\n`);
};

const setupSemanticRelease = (): void => {
  // tslint:disable-next-line: no-console
  console.log(`Setting up ${semanticRelease}...`);

  try {
    fileSystem.copyFileSync(
      `${defaults}/${semanticReleaseConfiguration}`,
      semanticReleaseConfiguration
    );
    fileSystem.copyFileSync(
      `${defaults}/${gitLabConfiguration}`,
      gitLabConfiguration
    );
  } catch (error) {
    throw error;
  }

  // tslint:disable-next-line: no-console
  console.log(`Successfully setup ${semanticRelease}. 🚀\n`);
};
