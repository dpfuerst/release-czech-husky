import chalk from 'chalk';

export const gitCZ: string = chalk.italic('git-cz');
export const husky: string = chalk.italic('husky');
export const releaseCzechHusky: string = chalk.bold('release-czech-husky');
export const semanticRelease: string = chalk.italic('semantic-release');
