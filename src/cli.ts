#!/usr/bin/env node --experimental-modules

import commandLineArgs, { CommandLineOptions } from 'command-line-args';
import install from './install';
import update from './update';

const cli = async (): Promise<void> => {
  const commandPlaceholder = [{ name: 'command', defaultOption: true }];

  const actualCommand: CommandLineOptions = commandLineArgs(
    commandPlaceholder,
    {
      stopAtFirstUnknown: true
    }
  );
  const actualCommandOptions: string[] = actualCommand._unknown || [];

  if (actualCommand.command === 'install') {
    await install();
  } else if (actualCommand.command === 'update') {
    await update(actualCommandOptions);
  } else {
    throw new Error(`Unknown command: ${actualCommand.command}`);
  }
};

// TODO: use Sentry instead of console logs.
// tslint:disable-next-line: no-console
cli().catch(error => console.error(error));
