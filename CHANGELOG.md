# Changelog

### [1.2.2](https://gitlab.com/dpfuerst/release-czech-husky/compare/v1.2.1...v1.2.2) (2020-02-22)


### Styles

* **npm:** 💄 order package.json keys alphabetically ([3a2b92a](https://gitlab.com/dpfuerst/release-czech-husky/commit/3a2b92af1875cb4da03f009b7e93cbc9228c3ee1))


### Miscellaneous Chores

* **git:** 🤖 format and lint source code on commit ([3c4cd9d](https://gitlab.com/dpfuerst/release-czech-husky/commit/3c4cd9d86fb183801605e83775ac1adaac33f8f1))
* **macOS:** 🤖 change Git hook to use Git Tower ("Tower") ([360a9a0](https://gitlab.com/dpfuerst/release-czech-husky/commit/360a9a0572241cf69fe99ca25122e7fe7e93cc3e)), closes [#19](https://gitlab.com/dpfuerst/release-czech-husky/issues/19)


### Continuous Integration

* **gitlab:** 🎡 fix command sudo not found ([6aa5374](https://gitlab.com/dpfuerst/release-czech-husky/commit/6aa5374ffeed4d8dab21d892799a36769c604739))
* **npm:** 🎡 fix insufficient permission to run build ([6030f68](https://gitlab.com/dpfuerst/release-czech-husky/commit/6030f68bde7f3f0baca48add82c66949a13db176)), closes [#20](https://gitlab.com/dpfuerst/release-czech-husky/issues/20) [#20](https://gitlab.com/dpfuerst/release-czech-husky/issues/20)

### [1.2.1](https://gitlab.com/dpfuerst/release-czech-husky/compare/v1.2.0...v1.2.1) (2020-01-27)


### Bug Fixes

* **cli-update:** 🐛 add missing GitLab CI/CD configuration ([526f4e3](https://gitlab.com/dpfuerst/release-czech-husky/commit/526f4e3b96ee246f5a0683d41d3fe292983cc1c3)), closes [#18](https://gitlab.com/dpfuerst/release-czech-husky/issues/18)
* **cli-update:** 🐛 explicitly check variable for function type ([46a40b2](https://gitlab.com/dpfuerst/release-czech-husky/commit/46a40b255f61b1f79ffe2782af3fbf7373719f31))


### Documentation

* **README:** ✏️ document update process ([b029b02](https://gitlab.com/dpfuerst/release-czech-husky/commit/b029b026712fb19d61c605a9ca920890ca6ffd60)), closes [#17](https://gitlab.com/dpfuerst/release-czech-husky/issues/17)
* **README:** ✏️ fix missing command during installation ([6124df7](https://gitlab.com/dpfuerst/release-czech-husky/commit/6124df7b52323373f410a491dba18d64d1279c6c))


### Code Refactoring

* **cli-install:** 💡 convert to synchronous API ([9930503](https://gitlab.com/dpfuerst/release-czech-husky/commit/99305031d580523aabf4a0d91c2baea1544087b4)), closes [#15](https://gitlab.com/dpfuerst/release-czech-husky/issues/15)
* **npm:** 💡 convert source code to TypeScript ([b722648](https://gitlab.com/dpfuerst/release-czech-husky/commit/b722648c091e34a3f67293ab4782ec1e083c86ae))


### Miscellaneous Chores

* **git:** 🤖 add scopes for update and install commands ([7ffa36f](https://gitlab.com/dpfuerst/release-czech-husky/commit/7ffa36f1adb177221b9bebaa3b255b4275a4538b))
* **npm:** 🤖 add scripts for build automation ([dac62ba](https://gitlab.com/dpfuerst/release-czech-husky/commit/dac62ba9659a71afda36cf7e90382a441187c43c))
* **npm:** 🤖 fix TypeScript configuration to allow chaining ([a9fd8a3](https://gitlab.com/dpfuerst/release-czech-husky/commit/a9fd8a3cbd9222fff105d3649bdb85fc05b5cede))
* **npm:** 🤖 install linter for TypeScript ([5ec7b3c](https://gitlab.com/dpfuerst/release-czech-husky/commit/5ec7b3ccac54f260b15eda1c31bb0513f5d764c3))
* **npm:** 🤖 install Prettier 1.19.1 ([e03fe34](https://gitlab.com/dpfuerst/release-czech-husky/commit/e03fe34bbefd8e6ee5a29fdf373d9dcd8eb30cfd))
* **npm:** 🤖 install TypeScript 3.7.5 ([656b72e](https://gitlab.com/dpfuerst/release-czech-husky/commit/656b72edb6c1df7f1ef89f52613f53ad888a096f))
* **npm:** 🤖 tidy directory structure ([9df8e55](https://gitlab.com/dpfuerst/release-czech-husky/commit/9df8e55b1bc1d407aac8a055fa852bc81ee7dfab))
* **npm:** 🤖 whitelist package files instead of blacklist ([cc0ed28](https://gitlab.com/dpfuerst/release-czech-husky/commit/cc0ed28851dcae22ff9d866b9079cbfeeb613e61))

## [1.2.0](https://gitlab.com/dpfuerst/release-czech-husky/compare/v1.1.3...v1.2.0) (2020-01-25)


### Features

* **cli:** ✨ add command to update this package ([e070b59](https://gitlab.com/dpfuerst/release-czech-husky/commit/e070b5920bd6ef32c9a185dbab0f560827ee5e21))
* **cli:** ✨ add routine to update this package ([6f7b85b](https://gitlab.com/dpfuerst/release-czech-husky/commit/6f7b85bb28b39bb4361d8b7e4db7601a497b443e))


### Continuous Integration

* **gitlab:** 🎡 prevent unnecessary pipeline runs ([83132b1](https://gitlab.com/dpfuerst/release-czech-husky/commit/83132b135b48ae0d2925ae4a2ee4a286575da2d5)), closes [#11](https://gitlab.com/dpfuerst/release-czech-husky/issues/11)


### Documentation

* **README:** ✏️ document version bump in nested npm package ([33cfa4c](https://gitlab.com/dpfuerst/release-czech-husky/commit/33cfa4c56922f56c560c0b583da3107fde516486)), closes [#12](https://gitlab.com/dpfuerst/release-czech-husky/issues/12)
* **README:** ✏️ fix missing peer dependency for installation ([e123c00](https://gitlab.com/dpfuerst/release-czech-husky/commit/e123c00558b126f7d40e1e203423f034a6740b53))


### Miscellaneous Chores

* **git:** 🤖 add scope for CLI ([b38661f](https://gitlab.com/dpfuerst/release-czech-husky/commit/b38661f80654602d980c62d07cce41edec4acf25))
* **npm:** 🤖 install command-line-args 5.1.1 ([e090987](https://gitlab.com/dpfuerst/release-czech-husky/commit/e090987a8a7da0d730f13fe63997cf22b9ba2320))


### Improvement

* **cli:** 💎 add command to install this package ([99518cb](https://gitlab.com/dpfuerst/release-czech-husky/commit/99518cba86fb78a9ef41a6bfe06cd92e1e392006))


### Code Refactoring

* **cli:** 💡 separate files, folders, and formattings ([997afae](https://gitlab.com/dpfuerst/release-czech-husky/commit/997afaebab46f2237207c544938f0b574b41f3d2))

### [1.1.3](https://gitlab.com/dpfuerst/release-czech-husky/compare/v1.1.2...v1.1.3) (2020-01-21)


### Miscellaneous Chores

* **npm:** 🤖 add missing peer dependency ([2b6e4ea](https://gitlab.com/dpfuerst/release-czech-husky/commit/2b6e4ea5f11ed53feec2d1f3700c8238333ac69a))


### Documentation

* **README:** ✏️ add FAQ ([bc08401](https://gitlab.com/dpfuerst/release-czech-husky/commit/bc0840170c6515fddf79536158da484f1fbff4b8))
* **README:** ✏️ fix missing step during installation ([68a7677](https://gitlab.com/dpfuerst/release-czech-husky/commit/68a7677a9c4cfa8386d1dcbddfe443f91c4dbca8))
* **README:** ✏️ fix wrong package name ([24817a3](https://gitlab.com/dpfuerst/release-czech-husky/commit/24817a31b6538679994d7cacc06aab40267ed79b))

### [1.1.2](https://gitlab.com/dpfuerst/release-czech-husky/compare/v1.1.1...v1.1.2) (2020-01-20)


### Miscellaneous Chores

* **npm:** 🤖 align description with the one of README.md ([a01344f](https://gitlab.com/dpfuerst/release-czech-husky/commit/a01344f98dcc09e61ffad2615707171a19fce869)), closes [#7](https://gitlab.com/dpfuerst/release-czech-husky/issues/7)


### Documentation

* **README:** ✏️ document configuration of GitLab CI/CD ([0b98352](https://gitlab.com/dpfuerst/release-czech-husky/commit/0b98352a3d9ee3f8b9dbe40f4e58bebeb6eb8ed0))
* **README:** ✏️ document configuration of GitLab CI/CD Token ([f04c690](https://gitlab.com/dpfuerst/release-czech-husky/commit/f04c6906b62e72735dad45083f1fd13b1d45d0fe)), closes [#9](https://gitlab.com/dpfuerst/release-czech-husky/issues/9)
* **README:** ✏️ reference configuration file for git-cz ([9f6601f](https://gitlab.com/dpfuerst/release-czech-husky/commit/9f6601fc4a6ca6eec1eeea136ceccf35bb1f8038))

### [1.1.1](https://gitlab.com/dpfuerst/release-czech-husky/compare/v1.1.0...v1.1.1) (2020-01-18)


### Bug Fixes

* **npm:** 🐛 add missing peer dependency ([08bc05f](https://gitlab.com/dpfuerst/release-czech-husky/commit/08bc05f4819415822544076b6e285de162cf164d)), closes [#4](https://gitlab.com/dpfuerst/release-czech-husky/issues/4)
* **release:** 🐛 version in package-lock.json not updated ([df8d4c9](https://gitlab.com/dpfuerst/release-czech-husky/commit/df8d4c970961864f2a9143a2c903bfcf4d9e7168))


### Miscellaneous Chores

* **git:** 🤖 add scopes for documentation ([53b0b54](https://gitlab.com/dpfuerst/release-czech-husky/commit/53b0b543f47b85316cb05199dcc85b43eec299d4))
* **npm:** 🤖 fix version in package-lock.json ([ef21028](https://gitlab.com/dpfuerst/release-czech-husky/commit/ef210283d0142b1eefb9411aab5e619603befee8))
* **npm:** 🤖 simplify peer dependencies' version restrictions ([2c15544](https://gitlab.com/dpfuerst/release-czech-husky/commit/2c15544e5ff5e4791e4d82678c23adc8237ebf78))
* **npm:** 🤖 update git-cz to version 4.1.0 ([0893a6e](https://gitlab.com/dpfuerst/release-czech-husky/commit/0893a6e22198a71fa99231e4376b9270c363dff7))


### Documentation

* **CONTRIBUTING:** ✏️ add short contribution guide ([4fc66dc](https://gitlab.com/dpfuerst/release-czech-husky/commit/4fc66dc62776bbbebb8c6144e917c8e9b70b800f)), closes [#6](https://gitlab.com/dpfuerst/release-czech-husky/issues/6)
* **README:** ✏️ add short documentation ([7634772](https://gitlab.com/dpfuerst/release-czech-husky/commit/7634772b274391e18f1a663cbbb95f9021d1ec80)), closes [#2](https://gitlab.com/dpfuerst/release-czech-husky/issues/2)

## [1.1.0](https://gitlab.com/dpfuerst/release-czech-husky/compare/v1.0.0...v1.1.0) (2020-01-18)


### Features

* **npm:** ✨ configure release-czech-husky as npm package ([5f51b18](https://gitlab.com/dpfuerst/release-czech-husky/commit/5f51b18503ef1487f947ec6770211f0a90b10fa3))


### Bug Fixes

* **npm:** 🐛 configure this package as public ([c9da038](https://gitlab.com/dpfuerst/release-czech-husky/commit/c9da0386123fea9fbaffbd43dcd23a0907df5368))

## 1.0.0 (2020-01-17)


### Miscellaneous Chores

* **git:** 🤖 add hook before commit ([9331b3c](https://gitlab.com/dpfuerst/release-czech-husky/commit/9331b3c7290131ee0c8611e0e295c2701a14a211))
* **git:** 🤖 add list of ignored files ([6dae745](https://gitlab.com/dpfuerst/release-czech-husky/commit/6dae745b9c3638edd3dcbbac81bd6dfb7094ccec))
* **git:** 🤖 configure interactive prompt of git-cz ([eac7cbf](https://gitlab.com/dpfuerst/release-czech-husky/commit/eac7cbf2ae49b76c0c8a669c3fa08c9ff9359a16))
* **npm:** 🤖 add "Unlicense" license ([49fc700](https://gitlab.com/dpfuerst/release-czech-husky/commit/49fc70063deea8c937c131748ceac7e55df4fdcb))
* **npm:** 🤖 initialize npm package ([7c47875](https://gitlab.com/dpfuerst/release-czech-husky/commit/7c47875f93236c95055f36b6209f7e7d21e43cf9))
* **release:** 🤖 add configuration for automatic release ([50ca84b](https://gitlab.com/dpfuerst/release-czech-husky/commit/50ca84b3cc16e8756b3850a001ef8614e9ca97ed))
* **release:** 🤖 commit changelog on release ([83c99b2](https://gitlab.com/dpfuerst/release-czech-husky/commit/83c99b20901d9304fa803f3a94ebcfecfb8aab9e))
* **release:** 🤖 style changelog title as heading ([a1ff1ff](https://gitlab.com/dpfuerst/release-czech-husky/commit/a1ff1ff2f965b8cb58b5d56bdde2bcd8e8e149ef))


### Continuous Integration

* **gitlab:** 🎡 add CI pipeline configuration ([6ab9e2a](https://gitlab.com/dpfuerst/release-czech-husky/commit/6ab9e2abb68b6bc39dc857af824b2a652e1899bf))
* **gitlab:** 🎡 remove obsolete pipeline skip condition ([04c7146](https://gitlab.com/dpfuerst/release-czech-husky/commit/04c7146ec28a95fecbc1c79f226a0f090d66d7a3))
